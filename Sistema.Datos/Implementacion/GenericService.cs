﻿using Sistema.Datos.Interfaces;
using Sistema.Datos.DBContext;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Transactions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Sistema.Datos.Implementacion
{
    public class GenericService<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {

        private readonly PruebaVentaContext _dBContext;

        public GenericService(PruebaVentaContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task<TEntity> Crear(TEntity entidad)
        {
            try
            {
                _dBContext.Set<TEntity>().Add(entidad);
                
                await _dBContext.SaveChangesAsync();
                
                return entidad;

            } catch(Exception ex) {
                throw;
            }
        }

        public async Task<bool> Editar(TEntity entidad)
        {
            try
            {
                _dBContext.Set<TEntity>().Update(entidad);

                await _dBContext.SaveChangesAsync();

                return true;

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<bool> Eliminar(TEntity entidad)
        {
            try
            {
                _dBContext.Set<TEntity>().Remove(entidad);

                await _dBContext.SaveChangesAsync();

                return true;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<TEntity> Obtener(Expression<Func<TEntity, bool>> filtro, string[]? includes = null)
        {

            TEntity? entity;
            IQueryable<TEntity>? query;
            try
            {

                query = filtro == null ? _dBContext.Set<TEntity>() : _dBContext.Set<TEntity>()
                    .Where(filtro);

                if (includes != null)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }
                entity = query.AsNoTracking().FirstOrDefault();

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public async Task<IQueryable<TEntity>> Listar(Expression<Func<TEntity, bool>>? filtro = null, string[]? includes = null)
        {
            IQueryable<TEntity> query = filtro == null ? _dBContext.Set<TEntity>() : _dBContext.Set<TEntity>().Where(filtro);

            if (includes != null)
            {
                foreach (var entity in includes)
                {
                    query = query.Include(entity);
                }
            }

            return query;           
        }

    }
}
