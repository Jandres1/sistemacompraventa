﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Sistema.Datos.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<TEntity> Crear(TEntity entidad);
        Task<bool> Editar(TEntity entidad);
        Task<bool> Eliminar(TEntity entidad);
        Task<TEntity> Obtener(Expression<Func<TEntity, bool>> filtro, string[]? includes = null);
        Task<IQueryable<TEntity>> Listar(Expression<Func<TEntity, bool>>? filtro = null, string[]? includes = null);
    }
}
