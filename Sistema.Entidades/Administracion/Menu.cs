﻿using System;
using System.Collections.Generic;

namespace Sistema.Entidades.Administracion;

public partial class Menu
{
    public int Id { get; set; }
    public string Descripcion { get; set; } = null!;
    public int? MenuPadreId { get; set; }
    public bool? Activo { get; set; }
    public DateTime? FechaCreacion { get; set; }


    public virtual Menu? MenuPadre { get; set; }
}
