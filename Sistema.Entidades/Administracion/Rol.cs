﻿using System;
using System.Collections.Generic;

namespace Sistema.Entidades.Administracion;

public partial class Rol
{
    public int Id { get; set; }
    public string Descripcion { get; set; } = null!;
    public bool Activo { get; set; }
    public DateTime? FechaCreacion { get; set; }
}
