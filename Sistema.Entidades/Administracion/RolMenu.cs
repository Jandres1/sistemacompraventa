﻿using System;
using System.Collections.Generic;

namespace Sistema.Entidades.Administracion;

public partial class RolMenu
{
    public int Id { get; set; }

    public int? RolId { get; set; }

    public int? IdMenu { get; set; }

    public bool Activo { get; set; }

    public DateTime? FechaCreacion { get; set; }

    public virtual Menu? IdMenuNavigation { get; set; }

    public virtual Rol? Rol { get; set; }
}
