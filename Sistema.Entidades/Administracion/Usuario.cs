﻿using System;
using System.Collections.Generic;
using Sistema.Entidades.Ventas;

namespace Sistema.Entidades.Administracion;

public partial class Usuario
{
    public int Id { get; set; }
    public string Nombre { get; set; } = null!;
    public string? Correo { get; set; }
    public string Telefono { get; set; } = null!;
    public int? RolId { get; set; }
    public string? Clave { get; set; }
    public bool Activo { get; set; }
    public DateTime? FechaCreacion { get; set; }

    public virtual Rol? Rol { get; set; }
}
