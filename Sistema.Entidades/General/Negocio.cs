﻿using System;
using System.Collections.Generic;

namespace Sistema.Entidades.General;

public partial class Negocio
{
    public int Id { get; set; }

    public string Codigo { get; set; } = null!;

    public string Nombre { get; set; } = null!;

    public string? Direccion { get; set; }

    public string? Telefono { get; set; }
}
