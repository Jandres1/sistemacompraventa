﻿using System;
using System.Collections.Generic;

namespace Sistema.Entidades.Inventario;

public partial class CategoriaProducto
{
    public int Id { get; set; }
    public string Descripcion { get; set; } = null!;
    public bool Activo { get; set; }
    public decimal? Descuento { get; set; }
    public DateTime? FechaCreacion { get; set; }

    //public virtual ICollection<Producto> CatProductos { get; } = new List<Producto>();
}
