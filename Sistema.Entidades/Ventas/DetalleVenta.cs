﻿using Sistema.Entidades.Inventario;

namespace Sistema.Entidades.Ventas;

public partial class DetalleVenta
{
    public int Id { get; set; }

    public int VentaId { get; set; }

    public int ProductoId { get; set; }

    public string DescripcionProducto { get; set; } = null!;

    public decimal Cantidad { get; set; }

    public decimal Precio { get; set; }

    public decimal Total { get; set; }

    public virtual Producto Producto { get; set; } = null!;

    public virtual MaestroVenta Venta { get; set; } = null!;
}
