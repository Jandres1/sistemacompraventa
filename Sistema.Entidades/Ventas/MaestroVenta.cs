﻿using System;
using System.Collections.Generic;
using Sistema.Entidades.Administracion;

namespace Sistema.Entidades.Ventas;

public partial class MaestroVenta
{
    public int Id { get; set; }
    public string Numero { get; set; } = null!;
    public int? TipoDocumentoId { get; set; }
    public int? UsuarioId { get; set; }
    public string? NombreCliente { get; set; }
    public decimal SubTotal { get; set; }
    public decimal? Iva { get; set; }
    public decimal Total { get; set; }
    public DateTime? FechaCreacion { get; set; }


    public virtual TipoDocumento? TipoDocumento { get; set; }
    public virtual Usuario? Usuario { get; set; }
}
