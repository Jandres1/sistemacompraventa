﻿using System;
using System.Collections.Generic;

namespace Sistema.Entidades.Ventas;

public partial class TipoDocumento
{
    public int Id { get; set; }
    public string? Descripcion { get; set; }
    public bool? Activo { get; set; }
    public DateTime? FechaCreacion { get; set; }
}
