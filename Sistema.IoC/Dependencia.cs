﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sistema.Datos.DBContext;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos.Interfaces;
using Sistema.Datos.Implementacion;
using Sistema.Negocio.Interfaces;
using Sistema.Negocio.Implementacion;

namespace Sistema.IoC
{
    public static class Dependencia
    {
        public static void InyectarDependencias(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<PruebaVentaContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("CadenaSQL"));
            });

            services.AddTransient(typeof(IGenericRepository<>), typeof(GenericService<>));

            services.AddScoped<IRolNRepository, RolNService>();
            services.AddScoped<IUsuarioNRepository, UsuarioNService>();
            services.AddScoped<ICategoriaProductoNRepository, CategoriaProductoNService>();
            services.AddScoped<IProductoNRepository, ProductoNService>();
            services.AddScoped<IVentaNRepository, VentaNService>();
        }
    }
}
