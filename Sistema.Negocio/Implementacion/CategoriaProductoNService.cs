﻿using Sistema.Datos.Interfaces;
using Sistema.Entidades.Inventario;
using Sistema.Negocio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Implementacion
{
    public class CategoriaProductoNService : ICategoriaProductoNRepository
    {
        private readonly IGenericRepository<CategoriaProducto> _genericRepository;


        public CategoriaProductoNService(IGenericRepository<CategoriaProducto> genericRepository)
        {
            _genericRepository=genericRepository;
        }

        public async Task<List<CategoriaProducto>> ListarAsync(bool cargarReferencias = false)
        {
            List<string> includes = new();

            IQueryable<CategoriaProducto> query = await _genericRepository.Listar();
            return query.ToList();
        }
    }
}
