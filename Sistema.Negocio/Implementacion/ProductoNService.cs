﻿using Sistema.Datos.Interfaces;
using Sistema.Entidades.Inventario;
using Sistema.Negocio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Implementacion
{
    public class ProductoNService : IProductoNRepository
    {
        private readonly IGenericRepository<Producto> _gProductos;

        public ProductoNService(IGenericRepository<Producto> gProductos)
        {
            _gProductos=gProductos;
        }

        public async Task<Producto> AgregarAsync(Producto entity)
        {
            try
            {
                Producto existe = await _gProductos.Obtener(x => x.Codigo == entity.Codigo);

                if (existe != null)
                {
                    throw new TaskCanceledException($"Ya existe un producto con el código: {entity.Codigo}");
                }

                Producto newRow = await _gProductos.Crear(entity);

                if (newRow.Id == 0)
                {
                    throw new TaskCanceledException($"No se pudo agregar el nuevo registro: {entity.Codigo}");
                }

                return newRow;
            }
            catch (TaskCanceledException ex)
            {
                throw new Exception("Tarea Cancelada: " + ex.Message);
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> EditarAsync(Producto entity)
        {
            try
            {
                Producto existe = await _gProductos.Obtener(x => x.Codigo == entity.Codigo && x.Id != entity.Id);

                if (existe != null)
                {
                    throw new TaskCanceledException($"Ya existe un producto con el código: {entity.Codigo}");
                }

                
                bool editRow = await _gProductos.Editar(entity);

                if (editRow == false)
                {
                    throw new TaskCanceledException($"No se pudo editar el registro: {entity.Codigo}");
                }

                return editRow;
            }
            catch (TaskCanceledException ex)
            {
                throw new Exception("Tarea Cancelada: " + ex.Message);
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> EliminarAsync(int id)
        {
            try
            {
                Producto existe = await _gProductos.Obtener(x => x.Id == id);

                if (existe == null)
                {
                    throw new TaskCanceledException("El registro no existe..!");
                }

                bool resp = await _gProductos.Eliminar(existe);

                if (resp == false)
                {
                    throw new TaskCanceledException("No se pudo eliminar el registro..!");
                }

                return resp;
            }
            catch (TaskCanceledException ex)
            {
                throw new Exception("Tarea Cancelada: " + ex.Message);
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<Producto>> ListarAsync(bool cargarReferencias = false)
        {
            List<string> includes = new();

            if (cargarReferencias)
            {
                includes.Add("CategoriaProducto");
            }

            IQueryable<Producto> query = await _gProductos.Listar(null, includes.ToArray());
            return query.ToList();
        }
    }
}
