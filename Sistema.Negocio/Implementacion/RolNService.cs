﻿using Sistema.Datos.Interfaces;
using Sistema.Entidades.Administracion;
using Sistema.Negocio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Implementacion
{
    public class RolNService : IRolNRepository
    {
        private readonly IGenericRepository<Rol> _genericRepository;

        public RolNService(IGenericRepository<Rol> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public async Task<List<Rol>> Listar(bool cargarReferencias = false)
        {
            try
            {
                List<string> includes = new();

                //if (cargarReferencias)
                //{
                //    includes.Add("");
                //}

                IQueryable<Rol> query = await _genericRepository.Listar(null, includes.ToArray());
                return query.ToList();
            }
            catch
            {
                throw;
            }
        }
    }
}
