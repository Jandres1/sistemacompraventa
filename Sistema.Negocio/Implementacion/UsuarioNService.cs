﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Net;
using Sistema.Datos.Interfaces;
using Sistema.Entidades.Administracion;
using Sistema.Negocio.Interfaces;

namespace Sistema.Negocio.Implementacion
{
    public class UsuarioNService : IUsuarioNRepository
    {
        private readonly IGenericRepository<Usuario> _genericRepository;
    
        public UsuarioNService(IGenericRepository<Usuario> genericRepository)
        {
            _genericRepository=genericRepository;
        }

        public async Task<Usuario> Crear(Usuario usuario)
        {
            try
            {
                Usuario result = await _genericRepository.Crear(usuario);

                if (result.Id == 0)
                    throw new TaskCanceledException("No se pudo crear el usuario");

                return result;
            }
            catch(TaskCanceledException ex)
            {
                throw new Exception("Tarea cancelada");
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> Editar(Usuario usuario)
        {
            try
            {
                bool result = await _genericRepository.Editar(usuario);

                if (result == false)
                    throw new TaskCanceledException("No se pudo editar el usuario");

                return result;
            }
            catch (TaskCanceledException ex)
            {
                throw new Exception("Tarea cancelada");
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> Eliminar(int id)
        {
            try
            {
                Usuario existe = await _genericRepository.Obtener(x => x.Id == id);

                if (existe == null)
                {
                    throw new TaskCanceledException("El registro no existe..!");
                }

                bool result = await _genericRepository.Eliminar(existe);

                if (result == false)
                    throw new TaskCanceledException("No se pudo eliminar el usuario");

                return result;
            }
            catch (TaskCanceledException ex)
            {
                throw new Exception("Tarea cancelada");
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<Usuario>> Listar(bool cargarReferencias = false)
        {
            try
            {
                List<string> includes = new ();

                if(cargarReferencias)
                {
                    includes.Add("Rol");
                }

                IQueryable<Usuario> query = await _genericRepository.Listar(null, includes.ToArray());
                return query.ToList();
            }
            catch
            {
                throw;
            }
        }

        public async Task<Usuario> ObtenerPorId(int id, bool cargarReferencias = false)
        {
            try
            {
                List<string> includes = new ();

                if (cargarReferencias)
                {
                    includes.Add("Rol");
                }

                Usuario result = await _genericRepository.Obtener(x => x.Id == id, includes.ToArray());

                return result;
            }
            catch
            {
                throw;
            }
        }
    }


}
