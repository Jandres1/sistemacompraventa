﻿using Sistema.Datos.Interfaces;
using Sistema.Entidades.Ventas;
using Sistema.Negocio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Implementacion
{
    public class VentaNService : IVentaNRepository
    {
        private readonly IGenericRepository<MaestroVenta> _gMaestroVentas;
        private readonly IGenericRepository<DetalleVenta> _gDetalleVentas;

        public VentaNService(IGenericRepository<MaestroVenta> gMaestroVentas,
            IGenericRepository<DetalleVenta> gDetalleVentas)
        {
            _gMaestroVentas = gMaestroVentas;
            _gDetalleVentas = gDetalleVentas;
        }

        public async Task<MaestroVenta> AgregarAsync(MaestroVenta entity)
        {
            try
            {
                MaestroVenta newRow = await _gMaestroVentas.Crear(entity);

                if (newRow.Id == 0)
                {
                    throw new TaskCanceledException($"No se pudo agregar el nuevo registro: {entity.FechaCreacion}");
                }

                return newRow;
            }
            catch (TaskCanceledException ex)
            {
                throw new Exception("Tarea Cancelada: " + ex.Message);
            }
            catch
            {
                throw;
            }
        }

        public async Task<DetalleVenta> AgregarDetalleAsync(DetalleVenta entity)
        {
            try
            {
                DetalleVenta newRow = await _gDetalleVentas.Crear(entity);

                if (newRow.Id == 0)
                {
                    throw new TaskCanceledException("No se pudo agregar el nuevo registro");
                }

                return newRow;
            }
            catch (TaskCanceledException ex)
            {
                throw new Exception("Tarea Cancelada: " + ex.Message);
            }
            catch
            {
                throw;
            }
        }
    }
}
