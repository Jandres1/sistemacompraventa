﻿using Sistema.Entidades.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Interfaces
{
    public interface ICategoriaProductoNRepository
    {
        Task<List<CategoriaProducto>> ListarAsync(bool cargarReferencias = false);
    }
}
