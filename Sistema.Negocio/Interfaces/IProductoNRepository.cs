﻿using Sistema.Entidades.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Interfaces
{
    public interface IProductoNRepository
    {
        Task<Producto> AgregarAsync(Producto entity);
        Task<bool> EditarAsync(Producto entity);
        Task<bool> EliminarAsync(int id);

        Task<List<Producto>> ListarAsync(bool cargarReferencias = false);
    }
}
