﻿using Sistema.Entidades.Administracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Interfaces
{
    public interface IRolNRepository
    {
        Task<List<Rol>> Listar(bool cargarReferencias = false);
    }
}
