﻿using Sistema.Entidades.Administracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Interfaces
{
    public interface IUsuarioNRepository
    {
        Task<Usuario> Crear(Usuario usuario);
        Task<bool> Editar(Usuario usuario);
        Task<List<Usuario>> Listar(bool cargarReferencias = false);
        Task<bool> Eliminar(int id);
        Task<Usuario> ObtenerPorId(int id, bool cargarReferencias = false);
        
    }
}
