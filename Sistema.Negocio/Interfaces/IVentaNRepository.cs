﻿using Sistema.Entidades.Ventas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.Negocio.Interfaces
{
    public interface IVentaNRepository
    {
        Task<MaestroVenta> AgregarAsync(MaestroVenta entity);
        Task<DetalleVenta> AgregarDetalleAsync(DetalleVenta entity);        
    }
}
