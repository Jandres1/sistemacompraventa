﻿using AutoMapper;
using Sistema.WebApp.Models.ViewModels;
using Sistema.WebApp.Models;
using Sistema.WebApp.Utilidades.Response;
using Sistema.Negocio.Interfaces;
using Sistema.Entidades.Administracion;
using Microsoft.AspNetCore.Mvc;

namespace Sistema.WebApp.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly IRolNRepository _rolNRepository;
        private readonly IUsuarioNRepository _usuarioNRepository;
        private readonly IMapper _mapper;

        public UsuarioController(IRolNRepository rolNRepository, IUsuarioNRepository usuarioNRepository, IMapper mapper)
        {
            _rolNRepository = rolNRepository;
            _usuarioNRepository = usuarioNRepository;
            _mapper = mapper;
        }

        public IActionResult Index()
        {

            var lst = _usuarioNRepository.Listar(true);

            IEnumerable<Usuario> lstVM = _mapper.Map<IEnumerable<Usuario>>(lst.Result);

            return View(lstVM);
        }
    }
}
