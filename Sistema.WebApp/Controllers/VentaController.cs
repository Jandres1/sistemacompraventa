﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Sistema.Entidades.Inventario;
using Sistema.Entidades.Ventas;
using Sistema.Negocio.Interfaces;
using Sistema.WebApp.Models;
using Sistema.WebApp.Models.ViewModels;
using Sistema.WebApp.Utilidades.Response;
using System.Transactions;

namespace Sistema.WebApp.Controllers
{
    public class VentaController : Controller
    {
        private readonly IVentaNRepository _ventaNRepository;
        private readonly IProductoNRepository _productoNRepository;
        private readonly ICategoriaProductoNRepository _categoriaProductoNRepository;                
        private readonly IMapper _mapper;

        public VentaController(IVentaNRepository ventaNRepository, IProductoNRepository productoNRepository, ICategoriaProductoNRepository categoriaProductoNRepository, IMapper mapper)
        {
            _ventaNRepository = ventaNRepository;
            _productoNRepository = productoNRepository;
            _categoriaProductoNRepository = categoriaProductoNRepository;
            _mapper = mapper;
        }

        public IActionResult Index()
        {

            var lstProducts = _productoNRepository.ListarAsync();
            IEnumerable<ProductoVM> listProductsVM = _mapper.Map<List<ProductoVM>>(lstProducts.Result);

            // Generar items de productos
            if (listProductsVM != null)
            {
                listProductsVM = listProductsVM.Select(i => { i.Descripcion = $"{i.Descripcion} - [Precio: {i.Precio}]"; return i; }).ToList();
            }
            ViewBag.listProducts = listProductsVM;

            return View();
        }

        [HttpPost]
        public IActionResult Registrar([FromBody] AgregarVentaVM model)
        {
            GenericResponse<string> genericResponse = new GenericResponse<string>();

            try
            {                

                // Se valida que la compra tenga un detalle
                if (model.DetalleVentaModels == null || model.DetalleVentaModels.Count() < 1)
                {
                    genericResponse.Mensaje = "La venta no tiene detalle, Favor Validar..!";
                    genericResponse.Estado = false;
                    genericResponse.EstadoCode = StatusCodes.Status400BadRequest.ToString();

                    return StatusCode(StatusCodes.Status200OK, new { data = genericResponse });
                }

                using (TransactionScope scope = new TransactionScope())
                {                    

                    MaestroVentaModel maestroVenta = new()
                    {
                        Numero = model.Numero,
                        NombreCliente = model.NombreCliente,
                        SubTotal = model.Subtotal,
                        Total = model.Total,
                        FechaCreacion = DateTime.Now                        
                    };

                    var compraReg = _ventaNRepository.AgregarAsync(_mapper.Map<MaestroVenta>(maestroVenta)).Result;

                    // Validar si se obtuvo el ID del maestro de compra
                    if (compraReg == null || compraReg.Id < 1)
                    {
                        genericResponse.Mensaje = "No fue posible guardar el encabezado de la venta..!";
                        genericResponse.Estado = false;
                        genericResponse.EstadoCode = StatusCodes.Status204NoContent.ToString();

                        scope.Dispose();

                        return StatusCode(StatusCodes.Status200OK, new { data = genericResponse });
                    }

                    // Guardar detalle de la compra
                    foreach (var item in model.DetalleVentaModels)
                    {

                        DetalleVentaModel detalleVenta = new()
                        {
                            VentaId = compraReg.Id,
                            ProductoId = item.ProductoId,
                            Cantidad = item.Cantidad,
                            Total = item.Total,
                            Precio = item.Precio
                        };

                        var detallecompraReg = _ventaNRepository.AgregarDetalleAsync(_mapper.Map<DetalleVenta>(detalleVenta)).Result;

                        // Validar si se obtuvo el ID del maestro de compra
                        if (detallecompraReg == null || detallecompraReg.Id < 1)
                        {
                            genericResponse.Mensaje = "No fue posible guardar el encabezado de la venta..!";
                            genericResponse.Estado = false;
                            genericResponse.EstadoCode = StatusCodes.Status204NoContent.ToString();

                            scope.Dispose();

                            return StatusCode(StatusCodes.Status200OK, new { data = genericResponse });
                        }
                    }                    

                    // Finalizar transacción
                    scope.Complete();
                }

            }
            catch (TransactionAbortedException ex)
            {
                genericResponse.EstadoCode = StatusCodes.Status500InternalServerError.ToString();
                genericResponse.Estado = false;
                genericResponse.Mensaje = "Error Interno: [Fallo en Transacción] - Favor Validar..!";
            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;

                if (exMessage.Contains("Tarea Cancelada: "))
                {
                    genericResponse.EstadoCode = StatusCodes.Status500InternalServerError.ToString();
                    genericResponse.Estado = false;
                    genericResponse.Mensaje = "Información: " + ((ex.InnerException != null) ? ex.InnerException.Message : exMessage);

                    return StatusCode(StatusCodes.Status200OK, new { data = genericResponse });
                }

                genericResponse.EstadoCode = StatusCodes.Status500InternalServerError.ToString();
                genericResponse.Estado = false;
                genericResponse.Mensaje = "Error Interno Favor Validar..!";

            }

            return StatusCode(StatusCodes.Status200OK, new { data = genericResponse });
        }

    }
}
