﻿using Sistema.Entidades.Inventario;
using Sistema.Entidades.Ventas;

namespace Sistema.WebApp.Models
{
    public class DetalleVentaModel
    {
        public int Id { get; set; }

        public int VentaId { get; set; }

        public int ProductoId { get; set; }

        public string DescripcionProducto { get; set; } = null!;

        public decimal Cantidad { get; set; }

        public decimal Precio { get; set; }

        public decimal Total { get; set; }

        public virtual ProductoModel Producto { get; set; } = null!;

        public virtual MaestroVentaModel Venta { get; set; } = null!;
    }
}
