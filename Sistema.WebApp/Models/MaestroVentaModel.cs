﻿using Sistema.Entidades.Ventas;

namespace Sistema.WebApp.Models
{
    public class MaestroVentaModel
    {
        public int Id { get; set; }
        public string Numero { get; set; } = null!;
        public string NombreCliente { get; set; } = null!;
        public int? UsuarioId { get; set; }
        public decimal SubTotal { get; set; }
        public decimal? Iva { get; set; }
        public decimal Total { get; set; }
        public DateTime? FechaCreacion { get; set; }
    }
}
