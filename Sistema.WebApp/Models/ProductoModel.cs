﻿using Sistema.Entidades.Inventario;

namespace Sistema.WebApp.Models
{
    public class ProductoModel
    {
        public int Id { get; set; }

        public string Codigo { get; set; } = null!;

        public string Descripcion { get; set; } = null!;

        public int? CategoriaProductoId { get; set; }

        public decimal? Stock { get; set; }

        public decimal? Precio { get; set; }

        public bool Activo { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public virtual CategoriaProductoModel? CategoriaProducto { get; set; }
    }
}
