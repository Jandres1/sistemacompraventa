﻿using System.ComponentModel.DataAnnotations;

namespace Sistema.WebApp.Models
{
    public class RolModel
    {
        public int Id { get; set; }

        [Display(Name = "Descripción")]
        public string Descripcion { get; set; } = null!;

        public bool Activo { get; set; }
    }
}
