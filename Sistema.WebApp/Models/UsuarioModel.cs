﻿
namespace Sistema.WebApp.Models
{
    public class UsuarioModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; } = null!;
        public string? Correo { get; set; }
        public string Telefono { get; set; } = null!;
        public int? RolId { get; set; }
        public string? Clave { get; set; }
        public bool Activo { get; set; }
        public DateTime? FechaCreacion { get; set; }

        public virtual RolModel Rol { get; set; } = null!;
    }
}
