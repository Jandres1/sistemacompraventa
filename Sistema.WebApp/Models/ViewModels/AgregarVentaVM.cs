﻿namespace Sistema.WebApp.Models.ViewModels
{
    public class AgregarVentaVM
    {
        public string Numero { get; set; } = null!;
        public string NombreCliente { get; set; } = null!;
        public decimal Subtotal { get; set; }
        public decimal Total { get; set; }


        public virtual IEnumerable<DetalleVentaModel>? DetalleVentaModels { get; set; }
    }
}
