﻿namespace Sistema.WebApp.Models.ViewModels
{
    public class CategoriaProductoVM
    {
        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public bool Activo { get; set; }
        public decimal? Descuento { get; set; }
        public DateTime? FechaCreacion { get; set; }
    }
}
