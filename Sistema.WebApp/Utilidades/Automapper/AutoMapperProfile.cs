﻿using Sistema.WebApp.Models.ViewModels;
using Sistema.Entidades;
using System.Globalization;
using AutoMapper;
using Sistema.Entidades.Administracion;
using Sistema.WebApp.Models;
using Sistema.Entidades.Inventario;
using Sistema.Entidades.Ventas;

namespace Sistema.WebApp.Utilidades.Automapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile() 
        {
            CreateMap<Rol, RolModel>().ReverseMap();
            CreateMap<Rol, ListaRolVM>().ReverseMap();
            CreateMap<Usuario, ListaUsuariosVM>().ReverseMap();
            CreateMap<Usuario, UsuarioModel>().ReverseMap();

            CreateMap<CategoriaProducto, CategoriaProductoModel>().ReverseMap();
            CreateMap<CategoriaProducto, CategoriaProductoVM>().ReverseMap();
            CreateMap<Producto, ProductoModel>().ReverseMap();
            CreateMap<Producto, ProductoVM>().ReverseMap();
            CreateMap<MaestroVenta, MaestroVentaModel>().ReverseMap();
            CreateMap<DetalleVenta, DetalleVentaModel>().ReverseMap();
        }
    }
}
