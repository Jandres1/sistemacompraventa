﻿const Items = document.getElementById('items');

const TemplateDetalle = document.getElementById('template-carrito').content;

const btnCerrarCompra = document.getElementById('btnCerrarCompra');
const btnAgregarP = document.getElementById('btnAddProduct');

// Host y fragment
const Fragment = document.createDocumentFragment();
const host = window.location.origin;

let hfPorcDescuento = document.getElementById('hfPorcDescuento');

$("#txtCantidad").on("change", function (e) {

    let precio = Number.parseFloat(document.getElementById("txtPrecio").value);

    if (precio > 0) {

        document.getElementById("txtTotal").value = Intl.NumberFormat('es-NI', { style: 'decimal', maximumFractionDigits: 2, minimumFractionDigits: 2, roundingIncrement: 5, }).format((this.value * precio));
    }
})

$("#txtPrecio").on("change", function (e) {

    let cantidad = Number.parseFloat(document.getElementById("txtCantidad").value);

    if (cantidad > 0) {

        document.getElementById("txtTotal").value = Intl.NumberFormat('es-NI', { style: 'decimal', maximumFractionDigits: 2, minimumFractionDigits: 2, roundingIncrement: 5, }).format((this.value * cantidad));
    }
})

// Asignar el evento para agregar carritos al boton de comprar
btnAgregarP.addEventListener('click', e => {
    addCar(e);
});

const addCar = e => {

    // Asignar el producto al tabla del carrito
    setCar();

    // Detener la propagación de eventos a todo el contenedor
    e.stopPropagation();
}

// Adaptar setCar
const setCar = async () => {

    let precio = Number.parseFloat(document.getElementById('txtPrecio').value);

    if (isNaN(precio) || precio <= 0) {

        SwalMSGInfo('Información', 'El precio debe ser mayor a 0..!');
        return;
    }
    
    // Validación de la cantidad
    let cantidad = Number.parseFloat(document.getElementById("txtCantidad").value);

    if (isNaN(cantidad) || cantidad <= 0) {

        SwalMSGInfo('Información', 'La cantidad debe ser mayor a 0..!');
        return;
    }

    // Datos del producto   
    const cbProducto = document.getElementById("cmbProductos");
    let productoID = Number.parseInt(cbProducto.value);
    let productoDescripcion = cbProducto.options[cbProducto.selectedIndex].text;

    let SubTotal = precio * cantidad;
    
    let Total = SubTotal + Iva;

    // crear objeto de detalle
    const detalle = {
        idCar: productoID.toString(),
        ProductoId: productoID,
        DescripcionProducto: productoDescripcion,
        Cantidad: cantidad,
        Precio: precio,
        Total: Total        
    }    

    // Asignar el objeto producto al objeto del carrito
    carrito[detalle.idCar] = { ...detalle };

    // pintar el carrito
    drawCar();

    document.getElementById("txtPrecio").value = '';
    document.getElementById("txtCantidad").value = '';
    document.getElementById("txtTotal").value = '';       
}

const drawCar = () => {

    Items.innerHTML = '';

    // Transformar en array el objeto carrito y Recorrer el array de objetos para pintar el carrito
    Object.values(carrito).forEach(item => {

        const clon = TemplateDetalle.cloneNode(true);

        clon.querySelector('th').textContent = item.idCar;
        clon.querySelectorAll('td')[0].textContent = item.DescripcionProducto;
        clon.querySelectorAll('td')[3].textContent = item.Cantidad;
        clon.querySelectorAll('td')[4].textContent = Intl.NumberFormat('es-NI', { style: 'decimal', maximumFractionDigits: 2, minimumFractionDigits: 2, roundingIncrement: 5, }).format(item.Precio);
        clon.querySelectorAll('td')[6].textContent = Intl.NumberFormat('es-NI', { style: 'decimal', maximumFractionDigits: 2, minimumFractionDigits: 2, roundingIncrement: 5, }).format(item.Total);
        
        Fragment.appendChild(clon);
    });

    // console.log(items);
    Items.appendChild(Fragment);
}

btnCerrarCompra.addEventListener('click', e => {
    Guardar();
});


const Guardar = async () => {

    // Validar si se han agregado productos
    if (Object.entries(carrito).length === 0) {
        SwalMSGInfo('Información', 'No se han agregado productos..!');
        return;
    }    

    let numero = document.getElementById('Numero').value;

    if (numero == "" || numero == null) {
        SwalMSGInfo('Información', 'El número de compra no puede estar vacío..!');
        return;
    }

    let nombre = document.getElementById('NombreCliente').value;

    if (nombre == "" || nombre == null) {
        SwalMSGInfo('Información', 'El número de compra no puede estar vacío..!');
        return;
    }    

    const MontoTotal = Object.values(carrito).reduce((acumula, { Total }) => acumula + Total, 0);    

    // crear objeto para el Maestro de Compra
    let objCompra = {
        Numero: numero,
        NombreCliente: nombre,
        SubTotal: MontoTotal,
        Total: MontoTotal
    }

    // Obtener el array del objeto detalle, solo los valores y agregar a un nuevo array
    let arryCarrito = Object.values(carrito);

    // Asignar el array a la propiedad del objeto que se enviará al controlador
    objCompra.DetalleVentaModels = arryCarrito;

    var accion = host + '/Venta/Registrar'

    const res = await fetch(accion, {
        method: 'POST', // or 'PUT',
        body: JSON.stringify(objCompra), // data can be `string` or {object}!
        //body: dataPrm,
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    });
    const result = await res.json();

    if (result.data.estado) {

        swal({
            title: "Información",
            text: result.data.mensaje,
            icon: "success",
            closeOnEsc: false,
            closeOnClickOutside: false,
            button: "Aceptar"
        })
            .then((resultado) => {
                if (resultado) {

                    // Vaciar el objeto
                    carrito = {};
                    arryCarrito = [];

                    drawCar();

                    let url = host + '/Dashboard/Index'
                    window.location.href = url;
                }
            });
    }
    else {
        SwalMSGError("Información", `${result.data.mensaje}`);
    }

}