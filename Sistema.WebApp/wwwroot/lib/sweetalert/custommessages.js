﻿
function SwalMSGUpdate(mensaje) {
    swal({
        title: "Actualización",
        text: mensaje,
        icon: "success",
        button: "OK",
    });
}

function SwalMSGRegister(mensaje) {
    swal({
        title: "Registro",
        text: mensaje,
        icon: "success",
        button: "OK",
    });
}

function SwalMSGError(operacion, mensaje) {
    swal({
        title: operacion,
        text: mensaje,
        icon: "error",
        button: "OK",
    });
}

function SwalMSGInfo(operacion, mensaje) {
    swal({
        title: operacion,
        text: mensaje,
        icon: "info",
        button: "OK",
    });
}